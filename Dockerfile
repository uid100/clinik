FROM gitpod/workspace-full

RUN apt-get update && apt-get install -y --no-install-recommends \
        wget \
        unzip \
        build-essential \
        cmake \
        libuv1-dev \
        libmicrohttpd-dev \
        libssl-dev \
        gcc-7 \
        g++-7 \
        libhwloc-dev \
        tar \
        curl \
        git \
        sudo \
        nano \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*