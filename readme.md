# Spring PetClinic Sample Application [![setup automated](https://img.shields.io/badge/Gitpod-ready_to_code-blue?logo=gitpod)](https://gitpod.io/from-referrer/)

Start coding in a [ready-to-code development environment](https://www.gitpod.io):

[![setup automated](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/from-referrer/)

## Understanding the Spring Petclinic application
See the presentation [here](https://speakerdeck.com/michaelisvy/spring-petclinic-sample-application).

![](https://cloud.githubusercontent.com/assets/838318/19727082/2aee6d6c-9b8e-11e6-81fe-e889a5ddfded.png)

Or you can run it from Maven directly using the Spring Boot Maven plugin. If you do this it will pick up changes that you make in the project immediately (changes to Java source files require a compile as well - most people use an IDE for this):

```
./mvnw spring-boot:run
```
